import java.util.ArrayList;
import java.util.Arrays;
public class Phonebook extends Contact{

    ArrayList<Contact> contacts = new ArrayList<>();

    public Phonebook(){};

    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }




}
