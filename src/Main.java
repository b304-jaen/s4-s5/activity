// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe", "+639152468596", "+639228547963", "Quezon City", "Makati City");
        Contact contact2 = new Contact("Jane Doe", "+639162148573", "+639173698541", "Caloocan City", "Pasay");

        phonebook.getContacts().add(contact1);
        phonebook.getContacts().add(contact2);

        if(phonebook.getContacts().isEmpty()) {
            System.out.println("Phonebook is empty");
        }
        for(Contact contact : phonebook.getContacts()){
            System.out.println(contact.getName());
            System.out.println("--------------------");
            System.out.println(contact.getName() + " has the following registered numbers:");
            System.out.println(contact.getContactNumber1());
            System.out.println(contact.getContactNumber2());
            System.out.println("-----------------------------------");
            System.out.println(contact.getName() + " has the following registered addresses:");
            System.out.println("my home in " + contact.getAddress1());
            System.out.println("my office in " + contact.getAddress2());
            System.out.println("===================================");
        }

    }
}